If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}

Function DE_Log([String] $logText) {
    $fixedLogText = "[DE::Installer] " + $logText

    Write-EventLog -LogName DarkEmpire -Source workscripts -Message $fixedLogText -EventId 0 -EntryType Information
    Write-Host($fixedLogText)
}

# �������� ������� �����
Write-Host("�������� ������� �����")

    New-EventLog -LogName DarkEmpire -Source workscripts -ErrorAction SilentlyContinue
    Limit-EventLog -OverflowAction OverWriteAsNeeded -MaximumSize 1024KB -LogName DarkEmpire

DE_Log("�������� ������� ����� ���������")

Function GetPath {

    Write-Host("������� ���� �� ����� ���� ���������� �����: ")

    $folderPath=read-host
    
    if ( (Test-Path -Path $folderPath) -eq $false ) {
        Write-Host("���� ����� �� ����������!")

        return GetPath
    }

    Write-Host("�� ������� � ����� ������? [Y-��] [N-���] [��������� - ��������� ���� ����]")

    $ex=read-host

    if ($ex -eq "Y") {
        Return $folderPath
    } elseif ($ex -eq "N") {
        Write-Host("�� ���������� �� ��������� �������")
        Pause
        break
    }

    return GetPath
}

$folderPath = GetPath
DE_Log("������� ����� ��� �����������: " + $folderPath)
Pause

$vetka = "HKCU:\DarkEmpire"
New-Item -Type Folder -Path "HKCU:\" -Name "DarkEmpire" -ErrorAction SilentlyContinue
New-ItemProperty -Path $vetka -Name "MoveFolder" -PropertyType String -Value $folderPath -ErrorAction SilentlyContinue


$folder_name = "dark_empire"
$current_dest = Convert-Path "$PsScriptRoot\.."

$final_dest = Join-Path $env:LOCALAPPDATA $folder_name

$file_name_clean = "clear_desktop.ps1"
$file_name_update = "run_on_upgrade.ps1"

$final_path_update = Join-Path $final_dest "scripts" 
$final_path_update = Join-Path $final_path_update $file_name_update

$final_path_clean = Join-Path $final_dest "scripts"
$final_path_clean = Join-Path $final_path_clean $file_name_clean

if ( (Test-Path -Path $final_dest) -eq $false ) {
    DE_Log("�������� ����� " + $folder_name + " � ����� " + $env:LOCALAPPDATA)
    New-Item -ItemType Directory -Name $folder_name -Path $env:LOCALAPPDATA
}

DE_Log("����������� ������� � " + $final_dest)
Copy-Item -Path $current_dest\* -Destination $final_dest -Force -Recurse -ErrorAction SilentlyContinue

# ������ � �����������

DE_Log("�������� ������ � ������������")

$Trigger = New-ScheduledTaskTrigger -AtLogOn
$User = "$env:USERDOMAIN\$env:USERNAME"

$Action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "-NoProfile -NoLogo -NonInteractive -ExecutionPolicy Bypass -Command `"& {$final_path_update}`" "
#Register-ScheduledTask -TaskName "DarkEmpire\UpdateScripts" -Trigger $Trigger -User $User -Action $Action -RunLevel Highest -Force

$Action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "-NoProfile -NoLogo -NonInteractive -ExecutionPolicy Bypass -Command `"& {$final_path_clean}`" "
#Register-ScheduledTask -TaskName "DarkEmpire\CleanDesktop" -Trigger $Trigger -User $User -Action $Action -RunLevel Highest -Force

Pause