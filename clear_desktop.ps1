# version 1.2
$path_desktop = [Environment]::GetFolderPath("Desktop")
$path_move_folder = Get-ChildItem 'HKCU:\' -Include "DarkEmpire" -Recurse | Get-ItemPropertyValue -Name "MoveFolder"
$is_leave_shortcuts = $true
$is_move_folder_shortcuts_require = $true
$shortcut_name = "����� � ��� �����"


Function DE_Log([String] $logText, [int16] $eventID) {
    Write-EventLog -LogName DarkEmpire -Source workscripts -Message $logText -EventId $eventID -EntryType Information
}

if ( (Test-Path -Path "$path_move_folder/$env:USERNAME") -eq $false) {
    New-Item -ItemType Directory -Force -Path "$path_move_folder/$env:USERNAME"
    DE_Log("�������� ����� ��� " + $env:USERNAME, 3)
}

$fileList = Get-ChildItem -Path $path_desktop

if ($is_leave_shortcuts -eq $true) {
    DE_Log("������ ����� �� ������� �����", 1)

    $fileList = $fileList | Where-Object -FilterScript {($_.Extension -notmatch ".lnk")}
}

$fileList = $fileList | Where-Object -FilterScript {($_.Name -notmatch $shortcut_name)}

$localUserPath = Join-Path $path_move_folder $env:USERNAME

foreach ($file in $fileList) {
    Move-Item -Path $file.PSPath -Destination "$localUserPath"
}

if ($fileList.Length > 0) {
    DE_Log("���������� " + $fileList.Length + " ������", 2)
}

if ($is_move_folder_shortcuts_require -eq $true) {
    $shortcut = Get-ChildItem -Path $path_desktop/$shortcut_name.lnk -ErrorAction SilentlyContinue

    if (-not $shortcut) {
        $target = $path_desktop+'/'+$shortcut_name+'.lnk'

        $WshShell = New-Object -comObject WScript.Shell
        $Shortcut = $WshShell.CreateShortcut($target)
        $Shortcut.TargetPath = "$localUserPath"
        $Shortcut.Save()
    }
}

