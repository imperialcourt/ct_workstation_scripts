# version 1.2
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}

$args = @("reset"; "--hard")

$gitPath = Convert-Path "$PsScriptRoot\..\git\bin\git.exe"

Set-Location -Path $PsScriptRoot

& $gitPath @args

$args = @("pull")

& $gitPath @args 

# ���������� ������� ������� 21.05.21
## �������� ������ �������
$taskUpdateScripts = Get-ScheduledTask -TaskName 'UpdateScripts' -TaskPath '\DarkEmpire\' -ErrorAction SilentlyContinue
if ($taskUpdateScripts) {
    Unregister-ScheduledTask -TaskName 'UpdateScripts' -TaskPath '\DarkEmpire\' -Confirm:$False
        
    Write-Host('DarkEmpire\UpdateScripts �������')
}

$taskCleanDesktop = Get-ScheduledTask -TaskName 'CleanDesktop' -TaskPath '\DarkEmpire\' -ErrorAction SilentlyContinue
if ($taskCleanDesktop) {
    Unregister-ScheduledTask -TaskName 'CleanDesktop' -TaskPath '\DarkEmpire\' -Confirm:$False

    Write-Host('DarkEmpire\CleanDesktop �������')
}

$newTaskStartup = Get-ScheduledTask -TaskName 'Startup' -TaskPath '\DarkEmpire\' -ErrorAction SilentlyContinue 
if (!$newTaskStartup) {
    $Trigger = New-ScheduledTaskTrigger -AtLogOn
    $User = "$env:USERDOMAIN\$env:USERNAME"
    
    $folder_name = "dark_empire"
    $file_name_startup = "startup.ps1"
    $final_dest = Join-Path $env:LOCALAPPDATA $folder_name
    $final_path_startup = Join-Path $final_dest "scripts"
    $final_file_startup = Join-Path $final_path_startup $file_name_startup

    $nirCmdPath = Join-Path $final_path_startup "nircmd.exe"

    # $Action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "-NoProfile -NoLogo -NonInteractive -ExecutionPolicy Bypass -Command `"& {$final_path_startup}`" "
    $Action = New-ScheduledTaskAction -Execute "$nirCmdPath" -Argument "execmd powershell `"$final_file_startup`""
    Register-ScheduledTask -TaskName "DarkEmpire\Startup" -Trigger $Trigger -User $User -Action $Action -RunLevel Highest -Force
    
    Write-Host("��������� ����� ������")
}